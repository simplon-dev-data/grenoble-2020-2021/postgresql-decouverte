![PostgreSQL Logo](https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/Postgresql_elephant.svg/1200px-Postgresql_elephant.svg.png)

# Activité Découverte PostgreSQL

> Utilisation d'un système de gestion de base de données relationnelle

## Objectifs

- Découvrir un serveur de base de données moderne
- Utiliser le langage SQL pour communiquer avec un serveur de base de données

## Modalités

Cette activité est **à réaliser individuellement**, mais **en réfléchissant en groupe** :

- Chaque apprenant doit compléter ce document sur sa propre branche nommée `prenom-nom`
- Penser à cocher les cases à chaque étape réalisée
- Compléter chaque emplacement marqué `[A COMPLETER]`

## Partie 1 : recherche d'information

- Qu'est-ce qu'un système de gestion de base de données relationnelle (_SGBDR_ ou _RDBMS_ en anglais) ?

`[A COMPLETER]`

- Quels sont les autres _SGBDR_ existants ?

`[A COMPLETER]`

- Quelles sont les différences/avantages/inconvénients entre SQLite et PostgreSQL ?

`[A COMPLETER]`

## Partie 2 : installation

Nous allons utiliser Docker pour démarrer un serveur PostgreSQL :

- [ ] Lancer la configuration [`docker-compose.yml`](docker-compose.yml) déjà prête à l'emploi :

```bash
sudo docker-compose up -d
```

- [ ] Vérifier que le service `postgres` sont lancés sans erreur :

```bash
sudo docker-compose ps
```

- [ ] Se connecter au serveur PostgreSQL :

```bash
sudo docker-compose exec postgres psql -h postgres -U postgres decouverte
Password for user postgres: test

psql (13.1 (Debian 13.1-1.pgdg100+1))
Type "help" for help.

decouverte=#
```

## Partie 3 : types de données

Une des forces d'un _SGBDR_ comme PostgreSQL réside dans les types de données offerts nativement.

Vous pouvez consulter la [documentation officielle](https://www.postgresql.org/docs/current/datatype.html)
pour prendre connaissance de tous les types de données disponibles dans Postgres.

- [ ] Créer une table `traffic` :

```sql
CREATE TABLE traffic (
    "ip_address" INET,
    "timestamp" TIMESTAMP WITH TIME ZONE,
    "method" VARCHAR(8),
    "resource" VARCHAR(256),
    "protocol" VARCHAR(16),
    "status_code" INTEGER,
    "response_size" INTEGER,
    "referer" TEXT,
    "browser_family" VARCHAR(32),
    "os_family" VARCHAR(32),
    "device_brand" VARCHAR(32),
    "is_mobile" BOOLEAN,
    "is_bot" BOOLEAN
);
```

- [ ] Vérifier que la table est bien créée avec les bons types de colonnes :

```sql
-- lister toutes les tables
\d

-- afficher les détails des tables
\d+

-- afficher les détails de la table `traffic`
\d+ traffic
```

- [ ] Insérer les données du fichier [traffic.csv](traffic.csv) dans la table `traffic` (vous pouvez ajouter des données supplémentaire au fichier CSV si nécessaire) :

```bash
sudo cp traffic.csv .docker/data/
```

```sql
COPY traffic
FROM '/data/traffic.csv' (FORMAT 'csv', DELIMITER ',', HEADER TRUE);
```

- [ ] Vérifier les données insérées dans la table `traffic` :

```sql
SELECT * FROM traffic;
```

- [ ] Filtrer les lignes correspondant uniquement à du traffic mobile :

```sql
SELECT *
FROM traffic
WHERE is_mobile;
```

- [ ] Filtrer les lignes correspondant uniquement à du traffic de bot :

```sql
SELECT *
FROM traffic
WHERE is_bot;
```

- [ ] Filtrer les lignes correspondant uniquement à du traffic mobile sur Android non-bot :

```sql
SELECT *
FROM traffic
WHERE is_mobile AND NOT is_bot AND os_family = 'Android';
```

- [ ] Nombre de requêtes groupées par seconde :

```sql
SELECT timestamp, COUNT(*) as count
FROM traffic
GROUP BY timestamp;
```

- [ ] Filtrer les lignes correspondant uniquement à du traffic depuis l'intervalle d'adresses IP "31.56.96/24" :

```sql
SELECT *
FROM traffic
WHERE ip_address << inet '31.56.96/24';
```

- [ ] Tenter de reproduire d'autres requêtes du Projet 1 ("Mesure d'audience sur le web")

### Observations

`[A COMPLETER]`

## Partie 4 : modélisation des relations

Un des principaux avantages d'utilisation d'un _SGBDR_ est son système de **relations**
entre différentes tables.

Pour lier 2 tables entre elles, il est nécessaire de définir des **clés**.
Les 2 types de clés principaux sont :

- La **clé primaire**
- La **clé étrangère**

- [ ] Rechercher la différence entre clé primaire et étrangère

`[A COMPLETER]`

Nous allons extraire certaines informations de la table `traffic` dans leur propres tables :

- `products` : Produits (nom, URL)
- `browsers` : Navigateurs (nom)
- `systems` : Systèmes d'exploitation (nom)
- `devices` : Appareils (nom)

Voici le **modèle entité-association** (ou _Entity-relationshipe model_ en anglais) :

![Diagramme ER Relations](img/er-diagram-relations.png)

Cela nous permet de lister dans une seule table les valeurs possibles des données servant à filtrer.
Egalement, cela nous permet d'ajouter des données supplémentaires (ex : nom des produits en plus de l'URL)
afin d'en faciliter l'exploitation.

- [ ] Créer les tables nécessaires :

```sql
DROP TABLE traffic;

CREATE TABLE products (
    "id" SERIAL,
    "name" VARCHAR(256),
    "url" VARCHAR(256),
    PRIMARY KEY("id")
);

CREATE TABLE browsers (
    "id" SERIAL,
    "name" VARCHAR(32),
    PRIMARY KEY("id")
);

CREATE TABLE systems (
    "id" SERIAL,
    "name" VARCHAR(32),
    PRIMARY KEY("id")
);

CREATE TABLE devices (
    "id" SERIAL,
    "name" VARCHAR(32),
    PRIMARY KEY("id")
);

CREATE TABLE traffic (
    "ip_address" INET,
    "timestamp" TIMESTAMP WITH TIME ZONE,
    "method" VARCHAR(8),
    "protocol" VARCHAR(16),
    "status_code" INTEGER,
    "response_size" INTEGER,
    "referer" TEXT,
    "is_mobile" BOOLEAN,
    "is_bot" BOOLEAN,
    "product_id" INTEGER,
    "browser_id" INTEGER,
    "system_id" INTEGER,
    "device_id" INTEGER,
    FOREIGN KEY ("product_id") REFERENCES products ("id"),
    FOREIGN KEY ("browser_id") REFERENCES browsers ("id"),
    FOREIGN KEY ("system_id") REFERENCES systems ("id"),
    FOREIGN KEY ("device_id") REFERENCES devices ("id")
);
```

- [ ] Insérer des données dans les table `products`, `browsers`, `systems` et `devices` :

```sql
INSERT INTO products("name", "url")
VALUES
    ('Product 1', '/product/1'),
    ('Product 2', '/product/2'),
    ('Product 3', '/product/3'),
    ('Product 4', '/product/4'),
    ('Product 5', '/product/5'),
    ('Product 6', '/product/6'),
    ('Product 7', '/product/7'),
    ('Product 8', '/product/8'),
    ('Product 9', '/product/9'),
    ('Product 10', '/product/10')
;

INSERT INTO browsers("name")
VALUES
    ('Googlebot'),
    ('Chrome Mobile'),
    ('Firefox'),
    ('Firefox Mobile'),
    ('bingbot'),
    ('Mobile Safari')
;

INSERT INTO systems("name")
VALUES
    ('Android'),
    ('Windows'),
    ('iOS'),
    ('Other')
;

INSERT INTO devices("name")
VALUES
    ('Spider'),
    ('Huawei'),
    ('Samsung'),
    ('Apple'),
    ('Generic'),
    ('Other')
;
```

- [ ] Vérifier l'insertion des valeurs dans les table :

```sql
SELECT * FROM products;
SELECT * FROM browsers;
SELECT * FROM systems;
SELECT * FROM devices;
```

- [ ] Insérer des valeurs dans la table `traffic` avec les relations aux identifiants des autres tables :

```sql
INSERT INTO traffic
VALUES
    ('66.249.66.194', '2019-01-22T03:56:14+03:30', 'GET', 'HTTP/1.1', 200, 30577, '-', FALSE, TRUE, 10, 1, 4, 1),
    ('31.56.96.51', '2019-01-22T03:56:16+03:30', 'GET', 'HTTP/1.1', 200, 5667, 'https://google.com', TRUE, FALSE, 1, 2, 1, 2),
    ('31.56.96.51', '2019-01-22T03:56:16+03:30', 'GET', 'HTTP/1.1', 200, 5379, 'https://google.com', TRUE,FALSE, 1, 2, 1, 2),
    ('40.77.167.129', '2019-01-22T03:56:17+03:30', 'GET', 'HTTP/1.1', 200, 1696, 'https://yahoo.com', TRUE, FALSE, 3, 2, 1, 3),
    ('91.99.72.15', '2019-01-22T03:56:17+03:30', 'GET', 'HTTP/1.1', 200, 41483, 'https://bing.com', FALSE, FALSE, 6, 3, 2, NULL),
    ('130.185.74.243', '2019-01-22T03:56:17+03:30', 'GET', 'HTTP/1.1', 200, 2654, 'https://bing.com', TRUE, FALSE, 1, 4, 1, 5),
    ('157.55.39.175', '2019-01-22T03:56:18+03:30', 'GET', 'HTTP/1.1', 200, 3688, 'https://google.com', FALSE, FALSE, 2, 3, 2, NULL),
    ('46.224.77.32', '2019-01-22T03:56:18+03:30', 'GET', 'HTTP/1.1', 200, 14776, '-', FALSE, TRUE, 8, 5, 4, 1),
    ('66.249.66.194', '2019-01-22T03:56:18+03:30', 'GET', 'HTTP/1.1', 200, 34277, '-', FALSE, TRUE, 3, 1, 4, 1),
    ('5.211.97.39', '2019-01-22T03:56:18+03:30', 'GET', 'HTTP/1.1', 200, 1695, 'https://yahoo.com', TRUE, FALSE, 5, 6, 3, 4)
;
```

- [ ] Vérifier les nouvelles valeurs de la table `traffic` :

```sql
SELECT * FROM traffic;
```

- [ ] Pour effectuer une requête à partir de plusieurs table, nous utilisons une **jointure**, par exemple pour compter le nombre de requêtes concernant le produit nommé "Produit 1" :

```sql
SELECT COUNT(*)
FROM traffic
JOIN products ON traffic.product_id = products.id
WHERE products.name = 'Product 1';
```

- [ ] Exercice : tenter de reproduire les requêtes de la partie précédente en utilisant des jointures quand cela est nécessaire

## Partie 5 : connexion en Python

Nous allons voir comment se connecter à PostgreSQL en Python,
en utilisant le package [`psycopg`](https://www.psycopg.org).

- [ ] Install le package Python `psycopg2-binary` :

```bash
pip install psycopg2-binary
```

- [ ] Créer un nouveau module Python nommé `data.py` avec le contenu suivant :

```python
import psycopg2
from psycopg2.extensions import parse_dsn
```

- [ ] Se connecter à la base postgreSQL :

```python
db_dsn = "postgres://postgres:test@localhost:5432/decouverte"
db_args = parse_dsn(db_dsn)
conn = psycopg2.connect(**db_args)
```

- [ ] Exécuter une requête SQL :

```python
cur = conn.cursor()

cur.execute("SELECT * FROM traffic")
all_traffic = cur.fetchall()
print(all_traffic)

cur.close()
conn.close()
```
